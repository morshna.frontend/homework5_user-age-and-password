function enterData(question){
    let answer;
    do{
        answer = prompt(question);
    } while (!answer || !isNaN(answer));
    return answer;
}

function createNewUser(firstName, lastName, birthday) {
    
    let newUser = {
        firstName,
        lastName,
        birthday,
        getAge: function(){
            let yearNow = new Date();
            let yearAge = this.birthday.slice(6, 10);
            return yearNow.getFullYear() - yearAge;
            
        },
        getPassword: function(){
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        
        }
    }
        return newUser;
}
    const user = createNewUser(enterData('Введите имя'), enterData('Введите фамилию'), enterData('Напишите вашу дату рождения (формате dd.mm.yyyy)'));
    console.log(user);
    console.log(user.getAge());
    console.log(user.getPassword());
